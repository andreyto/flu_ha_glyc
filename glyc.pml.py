from collections import OrderedDict
import os
from os.path import join as pjoin
from subprocess import check_call

all_sites_resi = [8,22,38,45,63,81,122,126,133,144,165,246,285,483]
## 45 did not work with Modbase - teared apart
head_sites_resi = [63,122,126,133,144,165,246,285]
exper_sites_resi = [8,22,38,81,165,285]

mut_resi = [128,142,144,145]

tpl_glyc_dat = """\
NAG NGLB {}
NAG 14bb 1
BMA 14bb 2
MAN 13ab 3
MAN 16ab 3
MAN 13ab 5
MAN 16ab 5
MAN 12aa 4
MAN 12aa 8
MAN 12aa 7
"""

def sel_connected_hetero(sel_start,sel_name_ret,bond_len=2.0):
    """Select heteroatoms connected to initial selection.
    Connected here means directly or indirectly through
    other hetero atoms. Initial selection may or may not
    be hetero. If it is hetero, it will be included in
    the result."""
    sel_tpl = "({}) extend 1 and het"
    sel_dist_tpl = "({}) expand {} and het"
    cmd.delete(sel_name_ret)
    cmd.select(sel_name_ret,sel_tpl.format(sel_start))
    cnt_sel = cmd.count_atoms(sel_name_ret)
    if cnt_sel == 0:
        cmd.select(sel_name_ret,sel_dist_tpl.format(sel_start,bond_len))
        cnt_sel = cmd.count_atoms(sel_name_ret)
    while True:
        cnt_sel_prev = cnt_sel
        cmd.select(sel_name_ret,sel_tpl.format(sel_name_ret))
        cnt_sel = cmd.count_atoms(sel_name_ret)
        assert cnt_sel >= cnt_sel_prev
        if cnt_sel == cnt_sel_prev:
            break
    return cnt_sel

def sel_str_sug_site_resi(resi):
    return "sug_{}".format(resi)

def sel_str_sug_sites_resi(resi):
    return " or ".join([sel_str_sug_site_resi(ri) for ri in resi])

def make_sugar_selections(sug_sites_resi=head_sites_resi):
    for resi in sug_sites_resi:
        sel_name = sel_str_sug_site_resi(resi)
        cnt_at = sel_connected_hetero("mod and resi {}".format(resi),sel_name)
        print "Residue #{} - {} connected sugar atoms".format(resi,cnt_at)

def sel_str_resi(resi):
    return "resi "+"+".join([str(x) for x in resi])

def show_putative_sites(resi):
    cmd.show_as("sphere","all")
    cmd.select("sel_all_put",sel_str_resi(resi))
    cmd.color("green","all")
    cmd.color("red","sel_all_put")
    cmd.color("orange","sel_all_put and {}".format(sel_str_resi(exper_sites_resi)))
    cmd.color("yellow","sel_all_put and name ND2")
    cmd.color("blue","sel_all_put and name CG")
    cmd.label()
    cmd.label("sel_all_put and name ND2",'"%s-%s" % (resn,resi)')

def add_sugar(sug_pdb,resi):
    #try:
    #    cmd.remove("sug")
    #except:
    #    pass
    cmd.load(sug_pdb,"sug")
    for ri in resi:
        cmd.fuse("sug and name C1", "prot and resi {} and name ND2".format(ri))

def res_ord_nums(sel):
    stored.res_ind = OrderedDict()
    cmd.iterate(sel,"stored.res_ind[resi]=resn")
    resi_nums = OrderedDict()
    for i,k in enumerate(stored.res_ind.keys()):
        resi_nums[k] = str(i + 1)
    to_resi = OrderedDict()
    for resi,iord in resi_nums.items():
        to_resi[iord] = resi
    return dict(to_ord=resi_nums,to_resi=to_resi)

def prep_modbase_input(prot_sel,resi,out_dir):
    """
    Prepare input files for uploading to Sali's Modbase Allosmod server
    http://modbase.compbio.ucsf.edu/allosmod/help.cgi?type=glyc
    glyc.dat file is described at the bottom of the help page.
    Seems like the second configuration letter (as in 14bb) is redundant. 
    It should be completely determined by the configuration of the preceding ring and the 
    position of the link in that ring (as the direction of O atom relative to the plane 
    appear to always interleave).
    Its reference paper for glycans
    http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3840220/

    Notes:
    It appears that the default amount of sampling is inadequate for modeling sugars.
    Sugars end up lying on their sides on the protein surface, instead of sticking out
    like mushrooms as expected.
    Use at least 5 rounds of minimization and sampling of "intermediate probability" 
    conformations.
    """
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    cmd.save(pjoin(out_dir,"prot.fasta"),prot_sel)
    cmd.save(pjoin(out_dir,"prot.pdb"),prot_sel)
    #AllosMod needs alignment numbering
    resi_nums = res_ord_nums(prot_sel)["to_ord"]
    with open(pjoin(out_dir,"glyc.dat"),"w") as out:
        for ri in resi:
            out.write(tpl_glyc_dat.format(resi_nums[str(ri)]))

def mod_to_templ_numbering(prot_sel,mod_pdb_in,mod_pdb_out):
    stored.to_resi = res_ord_nums(prot_sel)["to_resi"]    
    cmd.load(mod_pdb_in,"mod")
    ##chains are assigned already but we want to make sure
    cmd.alter("mod and not het","chain='A'")
    cmd.alter("mod and het","chain='B'")
    cmd.sort()
    cmd.rebuild()
    cmd.alter("mod and chain A","resi=stored.to_resi[resi]")
    cmd.sort()
    cmd.rebuild()
    cmd.save(mod_pdb_out,"mod")

def receptor_to_mod(mod_sel,prot_sel,receptor_pdb,mod_rec_pdb_out):
    ## receptor cialic acid must be in coords of prot
    cmd.load(receptor_pdb,"rec")
    cmd.align("{} and not het".format(mod_sel),prot_sel)
    cmd.alter("rec","chain='C'")
    cmd.sort()
    cmd.rebuild()
    cmd.save(mod_rec_pdb_out,"{} or rec".format(mod_sel))

def load_epitopes(tab_file):
    epi = OrderedDict()
    with open(tab_file,"r") as inp:
        for line in inp:
            line = line.strip()
            if line:
                words = line.split()
                epi[words[0]] = words[1:]
    return epi
            

def show_epitopes(prot_sel,epi_resi,epi_color,epi_names):
    for epi_name in epi_names:
        cmd.color(epi_color[epi_name],
                  "{} and {}".format(prot_sel,
                                     sel_str_resi(epi_resi[epi_name])))

def show_mut(prot_sel,mut_resi,sphere_transp=0,dot_radius=1,mode_mod="surface_transp"):
    if mut_resi:
        mut_sel = "{} and {}".format(prot_sel,
                                     sel_str_resi(mut_resi))
    else:
        mut_sel = "none" #results in empty selection
    print "mut_sel={}".format(mut_sel)
    cmd.select("mut",mut_sel)
    #cmd.set("dot_width",dot_width,"mut")
    cmd.set("sphere_transparency",sphere_transp,"mut")
    if mode_mod != "surface_transp":
        ##in non-transparent protein, make mutations stand out
        ##by increasing sphere radius and adding spikes (dots)
        cmd.set("dot_density",1,"mut")
        cmd.set("dot_radius",dot_radius,"mut")
        cmd.set("sphere_scale", 1.5, "mut")
        cmd.show("dots","mut")
    cmd.color("red","mut")
    cmd.show("spheres","mut")

def render():
    cmd.set("ray_opaque_background","off")
    cmd.set("antialias", 2)
    cmd.ray(2400,2400)

def montage_images(names,out_name):
    n_rows = len(names)
    n_cols = len(names[0])
    names_flat = []
    
    for row in names:
        for name in row:
            names_flat.append(name)

    cmd = "gm montage +frame +shadow +label -tile {}x{} -geometry +0+0".format(n_cols,n_rows).split()
    cmd = cmd + names_flat + [ out_name ]
    check_call(cmd)

def show_mod_rec(mod_rec_pdb,
                 mode_sug="sticks",
                 mode_mod="surface_transp",
                 sug_sites_resi=head_sites_resi,
                 epi_names=None,
                 mut_resi=mut_resi,
                 name="render",
                 do_render=False):

    print "Picture name: {}".format(name)

    do_sug_surface = False

    epi_color = dict(
        A = "slate",
        B = "forest",
        C = "deepteal",
        D = "raspberry",
        E = "lightmagenta"
        )

    epi_resi = load_epitopes("HAEpitopes_ResidueNumbersForStructure.txt")

    if epi_names is None:
        epi_names = epi_resi.keys()
    
    cmd.reinitialize()
    cmd.load(mod_rec_pdb,"mod_rec")

    cmd.select("mod","mod_rec and chain A")
    cmd.select("sug","mod_rec and chain B")
    cmd.select("rec","mod_rec and chain C")

    if sug_sites_resi:
        make_sugar_selections(sug_sites_resi=sug_sites_resi)
        cmd.remove("sug and not ({})".format(sel_str_sug_sites_resi(sug_sites_resi)))
    else:
        cmd.remove("sug")

    if name.startswith("3C.3"):
        ##special treatment - remove backbone from 142 to simulate
        ##mutation to G observed in this clade
        print "Removing 142 sidechain in 3C.3"
        cmd.remove("mod and resi 142 and ! n. n+ca+c+o")

    cmd.show_as("cartoon","mod")
    cmd.show_as("sticks","rec")

    cmd.set_bond("stick_radius",0.1,"sug")

    sel_surf = []
    if mode_sug=="surface_transp":
        ## remove default hetatm ignore flag for surface and mesh display
        cmd.flag("ignore","sug","clear")
        cmd.set("transparency", 0.5,"sug")
        cmd.show_as("sticks","sug")
        sel_surf.append("sug")
    elif mode_sug=="spheres_solid":
        cmd.set("sphere_transparency",0,"sug")
        cmd.show_as("spheres","sug")
    elif mode_sug=="spheres_transp":
        cmd.set("sphere_transparency",0.9,"sug")
        cmd.show_as("sticks","sug")
        cmd.show("spheres","sug")
    elif mode_sug=="sticks":
        cmd.show_as("sticks","sug")
    else:
        raise ValueError("Unknown parameter value for mode_sug: {}".format(mod_sug))
        
    if mode_mod=="surface_transp":
        cmd.set("transparency", 0.5,"mod")
        sel_surf.append("mod")
    elif mode_mod=="surface_solid":
        cmd.set("transparency", 0,"mod")
        sel_surf.append("mod")
    else:
        raise ValueError("Unknown parameter value for mode_mod: {}".format(mod_mod))

    if sel_surf:
        cmd.show("surface"," or ".join(sel_surf))

    cmd.bg_color("white")

    cmd.color("orange","sug")
    cmd.color("yellow","mod")
    cmd.color("cyan","rec")

    ## This avoids blending of cartoon color
    ## with surface color due to transparency,
    ## which was generating red that could be 
    ## confused with mutation red coloring
    cmd.set("cartoon_color","grey")

    show_epitopes(prot_sel="mod",epi_resi=epi_resi,epi_color=epi_color,epi_names=epi_names)

    show_mut(prot_sel="mod",mut_resi=mut_resi,mode_mod=mode_mod)

    if False and name == "3C.1":
        ##special treatment - show sug at 144 as more transparent to
        ##communicate its known lower glycosylation probability
        cmd.set("sphere_transparency",0.4,"sug_144")
        cmd.set("transparency", 0.2,"sug_144")

    cmd.select("none")

    ## to change view, rotate/move as you like with the mouse,
    ## then type get_view, copy and paste the output into here.
    ## Note: output will have a single set of round brackets, but
    ## you need double set `((` like below.
    cmd.set_view ((\
     0.820865273,   -0.136966273,    0.554453373,\
     0.552665114,    0.435282648,   -0.710691094,\
    -0.144002676,    0.889806449,    0.433007628,\
     0.000203147,   -0.001241922, -228.310043335,\
    -45.932106018,   21.140825272,   83.625816345,\
    129.076309204,  327.537994385,  -20.000000000 ))

    ## needed on Linux Pymol v.1.6 when we saving w/o ray
    cmd.refresh()

    if do_render:
        render()

    img_name = name+".png"
    cmd.png(img_name,dpi=300)

    return img_name

def restart_from_prot(prot_pdb):
    cmd.reinitialize()
    cmd.load(prot_pdb,"prot")


## entry points

def initial_analysis():
    restart_from_prot("prot.pdb")
    show_putative_sites(head_sites_resi)
    add_sugar("sug_test_34.pdb",head_sites_resi)

def prep_modbase():
    restart_from_prot("prot.pdb")
    prep_modbase_input("prot",head_sites_resi,"modbase/2014-10-08")


def after_modbase():
    """Run this after you downloaded and saved your simulation results from ModBase.
    Edit the path to Modbase model that you want to use.
    Modbase generates several models per run - some might have clear issues like
    glycan driven into the core of the protein. Look at several before making your
    choice, then edit the path to the model PDB below"""
    restart_from_prot("prot.pdb")
    mod_to_templ_numbering("prot",
                       mod_pdb_in="modbase/2014-10-08/result/pred_dECALCrAS1000/prot.pdb_1/pm.pdb.B99990001.pdb",
                       mod_pdb_out="mod.pdb")
    receptor_to_mod(mod_sel="mod",prot_sel="prot",receptor_pdb="receptor.pdb",mod_rec_pdb_out="mod_rec.pdb")


## 1. prep_modbase()
## 2. Upload prepared files to Modbase (see comments for prep_modbase_input()
## 3. Edit Modbase output structure path in after_modbase()
## 4. after_modbase()
## 5. Uncomment and possibly modify any of the display parameter lines in show_mods_rec,
##    as well as groups of glycans and mutations for each image panel
## 6. Run show_mods_rec(do_render=False)
## 7. show_mods_rec() will created a montage image called `combined.png`. Once satisfied 
##    with the picture, call show_mods_rec(do_render=True) to generate ray-traced high
##    quality picture (may take a while).

def show_all_features():
    kw = dict(mode_sug="spheres_solid",
              mode_mod="surface_transp",
              sug_sites_resi = head_sites_resi,
              mut_resi = mut_resi
             )
    show_mod_rec("mod_rec.pdb",
                 name="all_features",
                 **kw)

def show_mods_rec(do_render=False):
    #kw=dict(mode_sug="sticks",mode_mod="surface_transp")
    #kw=dict(mode_sug="surface_transp",mode_mod="surface_transp")
    #kw=dict(mode_sug="spheres_transp",mode_mod="surface_transp")
    #kw=dict(mode_sug="sticks",mode_mod="surface_solid")
    #kw=dict(mode_sug="spheres_transp",mode_mod="surface_solid")
    kw=dict(mode_sug="spheres_solid",mode_mod="surface_transp")
    
    kw["do_render"] = do_render

    groups = OrderedDict([
                  ("1", dict(
                             sug_sites_resi = [63,122,126,133,165,246,285],
                             mut_resi = []
                             )),
                  ("3C.1", dict(
                             sug_sites_resi = [63,122,126,133,144,165,246,285],
                             mut_resi = []
                             )),
                  ("3C.2", dict(
                             sug_sites_resi = [63,126,133,144,165,246,285],
                             mut_resi = [145]
                             )),
                  ("3C.3", dict(
                             sug_sites_resi = [63,122,133,144,165,246,285],
                             mut_resi = [128,142,145]
                             )),
                  ("3C.4", dict(
                             sug_sites_resi = [63,122,126,133,144,165,246,285],
                             mut_resi = [145]
                             )),
             ])

    img_names = []
    for gr_name in groups.keys():
        row_img_names = []
        k = kw.copy()
        k.update(groups[gr_name])
        row_img_names.insert(0,show_mod_rec("mod_rec.pdb",
                     name=gr_name+".2",
                     **k))
        k["sug_sites_resi"] = None
        row_img_names.insert(0,show_mod_rec("mod_rec.pdb",
                     name=gr_name+".1",
                     **k))
        img_names.append(row_img_names)

    montage_images(names=img_names,out_name="composed.png")

after_modbase()
show_mods_rec(do_render=True)
#show_all_features()

#montage_images(names=[["1.1.png","1.2.png"],["2.1.png","2.2.png"]],out_name="composed.png")
#prep_modbase()
