# Modeling and visualization of influenza hemagglutinin glycosylation #

This code helps in modeling glycosylation of influenza virus HA protein and 
showing on the protein structure the antibody epitopes, mutations and modeled glycans.

### Requirements ###

* Pymol 1.6 or later
* GraphicsMagic executables in the PATH

### How to run ###

* Open Pymol, making sure that the code directory is Pymol's current working directory 
  (start Pymol from the terminal in Linux while in the code directory; in Windows it 
  might work double-clicking on the `prot.pdb` or using Open With right-button menu 
  in file explorer; worst case in Windows, you will have to use `cd` command in Pymol 
  command line window)
* In Pymol command line, type `run glyc.pml.py`
* It will run for a long time (couple of hours) and generate a publication-quality figure 
  `composed.png` with 2x6 matrix of protein images
* To generate a figure with different input parameters (mutations, epitopes, colors), or 
  to build a model with different glycans using AllosMod, look for commented out instructions 
  at the bottom of `glyc.pml.py`.

### Authors ###

* Andrey Tovchigrechko <atovtchi@jcvi.org>
* Karla Stucker <kstucker@jcvi.org>

The script generates images using data obtained at J. Craig Venter Institute

### License ###

LGPL v.3

See file COPYING for license terms and copyright
